import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        title: 'CablePost'
      }
    },
    {
      path: '/mc',
      name: 'mc',
      component: () => import('@/views/mc/HomeView.vue'),
      meta: {
        title: 'Minecraft | CablePost'
      }
    },
    {
      path: '/mc/mods',
      name: 'mc-mods',
      component: () => import('@/views/mc/ModsView.vue'),
      meta: {
        title: 'Minecraft Mods | CablePost'
      }
    },
    {
      path: '/mc/skyblockbazaarmonitor/',
      name: 'skyblock-bazaar-monitor',
      component: () => import('@/views/mc/SkyBlockBazaarMonitor.vue'),
      meta: {
        title: 'SkyBlock Bazaar Monitor | CablePost'
      }
    },
    {
      path: '/bodkinracer/',
      name: 'bodkinracer',
      component: () => import('@/views/bodkinRacer/Index.vue'),
      meta: {
        title: 'BodkinRacer | CablePost'
      }
    },
    {
      path: '/school/timestables/',
      name: 'timestables',
      component: () => import('@/views/school/TimesTablesView.vue'),
      meta: {
        title: 'Times Tables | CablePost School'
      }
    },
    {
      path: '/:pathMatch(.*)',
      name: '404',
      component: () => import('@/views/PageNotFoundView.vue'),
      meta: {
        title: '404 | CablePost'
      }
    },
  ]
});

router.afterEach((to, from) => {
  document.title = to.meta.title || 'CablePost';
});

export default router
