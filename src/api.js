const API_BASE = "https://api.cablepost.co.uk";

export async function ApiPostJsonThrow(path, body){
	const response = await fetch(
		API_BASE + path,
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify(body),
		}
	);
	
	if (response.status === 404) {
		throw new Error("API Failed, 404 not found");
	}
	
	if (response.status === 500) {
		throw new Error("API Failed, 500 internal error");
	}
	
	if (response.status !== 200) {
		throw new Error((await response.json()).message);
	}
	
	return await response.json();
}